<?php include 'all/top.php' ;?>
<div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Usuarios del sistema</h3>
                            <p class="text-muted">Añadir <a href=""><code>usuario nuevo</code></a></p>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead style="text-align:center;">
                                        <tr>
                                            <th style="text-align:center;">#</th>
                                            <th style="text-align:center;">Nombre</th>
                                            <th style="text-align:center;">Apellido</th>
                                            <th style="text-align:center;">Correo</th>
                                            <th style="text-align:center;">Rol</th>
                                            <th style="text-align:center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align:center;">1</td>
                                            <td style="text-align:center;">Ismael</td>
                                            <td style="text-align:center;">Catala</td>
                                            <td style="text-align:center;">icatala@me.com</td>
                                            <td style="text-align:center;"><span class="label label-danger">Administrador</span> </td>
                                            <td style="text-align:center;"><span class="label label-info"><a href="" style="color:white;">Editar</a></span>

                                                                           <span class="label label-danger"><a href="" style="color:white;">Borrar</a></span> </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;">2</td>
                                            <td style="text-align:center;">Francisco</td>
                                            <td style="text-align:center;">Molina</td>
                                            <td style="text-align:center;">fmolina@me.com</td>
                                            <td style="text-align:center;"><span class="label label-warning">Técnico</span> </td>
                                            <td style="text-align:center;"><span class="label label-info"><a href="" style="color:white;">Editar</a></span>

                                                                           <span class="label label-danger"><a href="" style="color:white;">Borrar</a></span> </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;">3</td>
                                            <td style="text-align:center;">Rocio</td>
                                            <td style="text-align:center;">Garcia</td>
                                            <td style="text-align:center;">rgarcia@me.com</td>
                                            <td style="text-align:center;"><span class="label label-warning">Técnico</span> </td>
                                            <td style="text-align:center;"><span class="label label-info"><a href="" style="color:white;">Editar</a></span>

                                                                           <span class="label label-danger"><a href="" style="color:white;">Borrar</a></span> </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;">4</td>
                                            <td style="text-align:center;">Alejandro</td>
                                            <td style="text-align:center;">Pindando</td>
                                            <td style="text-align:center;">apindado@me.com</td>
                                            <td style="text-align:center;"><span class="label label-default">Usuario</span> </td>
                                            <td style="text-align:center;"><span class="label label-info"><a href="" style="color:white;">Editar</a></span>

                                                                           <span class="label label-danger"><a href="" style="color:white;">Borrar</a></span> </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                  </div>




<?php include 'all/footer.php';?>
