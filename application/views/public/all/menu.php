<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div><img src="https://es.gravatar.com/userimage/78876975/6b8596007f921cccd97d9e17acd59e92.jpg?size=200" alt="user-img" class="img-circle"></div> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ismael Catalá <span class="caret"></span></a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="#"><i class="ti-user"></i> Mi perfil</a></li>
                    <li><a href="#"><i class="ti-email"></i> Mensajes</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#"><i class="ti-settings"></i> Ajustes</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo base_url().'auth/logout';?>"><i class="fa fa-power-off"></i> Salir</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
    <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
    </span> </div>
                <!-- /input-group -->
            </li>
            <li class="nav-small-cap m-t-10">--- Menu Usuario</li>
            <li> <a href="/" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard </span></a>

            </li>

            <li class="nav-small-cap">--- Administración</li>
            <li> <a href="<?php echo base_url().'usuarios';?>" class="waves-effect active"><i data-icon="&#xe008;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Usuarios</span></a>

            </li>
            <li> <a href="/" class="waves-effect"><i data-icon="&#xe006;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">iPads</span></a>

            </li>
            <li> <a href="/" class="waves-effect"><i data-icon="O" class="linea-icon linea-software fa-fw"></i> <span class="hide-menu">Incidencias<span class="label label-rouded label-danger pull-right">7</span></span></a>

            </li>
            <li> <a href="/" class="waves-effect"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Notificaciones</span></a> </li>
            <li> <a href="/" class="waves-effect"><i data-icon="7" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Sistema</span></a>

            </li>
          </ul>
    </div>
</div>
<!-- Left navbar-header end -->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
              <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                      <h4 class="page-title">Dashboard</h4> </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                      <ol class="breadcrumb">
                          <li><a href="/">iPad Theme Manager</a></li>
                          <li class="active">Dashboard</li>
                      </ol>
                  </div>
                  <!-- /.col-lg-12 -->
              </div>
