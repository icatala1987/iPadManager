$( document ).ready(function() {



    var ctx3 = document.getElementById("chart3").getContext("2d");
    var data3 = [
        {
            value: 300,
            color:"#25a6f7",
            highlight: "#25a6f7",
            label: "David"
        },
        {
            value: 50,
            color: "#edf1f5",
            highlight: "#edf1f5",
            label: "Alejandro"
        },
		 {
            value: 50,
            color: "#b4c1d7",
            highlight: "#b4c1d7",
            label: "Rocio"
        },
		 {
            value: 50,
            color: "#b8edf0",
            highlight: "#b8edf0",
            label: "Ismael"
        },
        {
            value: 100,
            color: "#fcc9ba",
            highlight: "#fcc9ba",
            label: "Oliver"
        }
    ];

    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#96a2b4",
        segmentStrokeWidth : 0,
        animationSteps : 100,
		tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });

    var ctx4 = document.getElementById("chart4").getContext("2d");
    var data4 = [
        {
            value: 234,
            color:"#01c0c8",
            highlight: "#01c0c8",
            label: "Cerradas"
        },
        {
            value: 100,
            color: "#fb9678",
            highlight: "#fb9678",
            label: "Abiertas"
        }
    ];

    var myDoughnutChart = new Chart(ctx4).Doughnut(data4,{
        segmentShowStroke : true,
        segmentStrokeColor : "#96a2b4",
        segmentStrokeWidth : 0,
        animationSteps : 100,
		tooltipCornerRadius: 2,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });



});
